"use strict";

[1, 2, 3].map(function (n) {
  return Math.pow(n, 2);
});
var request = fetch('https://api.chucknorris.io/jokes/random').then(function (res) {
  return res.json();
}).then(function (result) {
  return console.log(result);
});